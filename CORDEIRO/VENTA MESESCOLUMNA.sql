select * from
(SELECT p.UniqueID,p.pnombre,datepart(month,fecha) Mes,sum(cantidad) Cantidad

FROM FacturaCabeceraB cc join FacturaCuerpoB cd on cc.IDAutomatico = cd.FacturaID
						join Productos p on cd.ProductoID = p.UniqueID
						join rubros_cab ru on p.Id_Rubro =ru.Id_Rubro

 where isnull(cc.Estado,0) = 0
       and cast(cc.Fecha as date) between '20190101' and '20191201'
	  group by datepart(month,fecha),p.UniqueID,p.pnombre
) s
pivot
(
 sum(cantidad) for s.mes in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
) piv




select * from
(SELECT p.UniqueID,p.pnombre,datepart(month,fecha) Mes,sum(cd.MontoTotal) monto

FROM FacturaCabeceraB cc join FacturaCuerpoB cd on cc.IDAutomatico = cd.FacturaID
						join Productos p on cd.ProductoID = p.UniqueID
						join rubros_cab ru on p.Id_Rubro =ru.Id_Rubro

 where isnull(cc.Estado,0) = 0
       and cast(cc.Fecha as date) between '20190101' and '20191201'
	  group by datepart(month,fecha),p.UniqueID,p.pnombre
) s
pivot
(
 sum(monto) for s.mes in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
) piv