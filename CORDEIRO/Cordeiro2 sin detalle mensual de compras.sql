--ALTER PROCEDURE planillacompra @fechaD varchar(20),@fechaH varchar(20) AS
 SET NOCOUNT ON 

DECLARE @fechaD varchar(20) = '20180101' ,@fechaH varchar(20) = '20181231'

DECLARE @FECHANT AS VARCHAR(20)

DROP TABLE  Temp_Stock

CREATE TABLE Temp_Stock
(
Agrupacion VARCHAR(50),
Id_Sucursal int,
Sucursal varchar(50) ,
Id_Deposito INT,
Deposito varchar(50),
Id_Rubro int,
Id_SubRubro int,
Id_Producto int,
CodigoEspecial int,
CodigoBarras float,
Descripcion varchar(50),
Cantidad float,
PrecioC float,
PesoU float,
Iva int,
Peso float,
MontoTotal int,
Rubro VARCHAR(50),
SubRubro VARCHAR(50)
)
--DECLARE @fechaD varchar(20) = '20190313 00:00:00' ,@fechaH varchar(20) = '20190313 23:00:00', @FECHANT AS VARCHAR(20)
INSERT  Temp_Stock  EXECUTE VerificarStock @P_FECHA = @fechaH;

SELECT
ProductoID Cod_Articulo, PNombre Descripcion,Sum(ene) Enero,sum(febr)Febrero, sum(mar) Marzo, sum(Abr)Abril,sum(May)Mayo, sum(jun) Junio,
		sum(jul)Julio,sum(ago) Agosto, sum(seti) Setiembre, sum(oct) Octubre, sum(nov) Noviembre, Sum(dic) Diciembre,0,0,sum(Stock) Stock,
		sum(Venta_Actual) Venta_Actual,MAX(ProvID)ProvID,MAX(ProvNom) ProvNom,MAX(T.id_rubro) id_rubro,max(t.RubroDesc) Rubro, sum(t.compra) compra ,max(t.PrecioC)PrecioC,max(t.PrecioU)PrecioU,max(t.CodigoEspecial)CodigoEspecial

from(
		select Id_Producto ProductoID  ,Descripcion PNombre

				,Cantidad Stock 
				
				,0 Ene,0 Febr,0 Mar,0 Abr,0 May,0 Jun,0 Jul,0 Ago,0 Seti,0 Oct,0 Nov,0 Dic,0 Venta_Actual

				,0 compra,0 provID,' '  ProvNom,0 id_rubro,'' RubroDesc,0 PrecioC, 0 PrecioU, 0 CodigoEspecial
				 
		from Temp_Stock
		where Id_Deposito = 3
				  and Id_Rubro = 10
		
		union all

		 select UniqueID , PNombre,0,[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],0

		 ,0 Compra,0 ProvID,' ' ProvNom, id_rubro,RubroDesc,0 PrecioC,  PrecioU,  CodigoEspecial

		from
		(
			select
			p.UniqueID, p.PNombre , DATEPART(MONTH ,cast(fc.fecha as date)) Mes,SUM(fd.Cantidad) Cantidad,AVG(fd.Preciou) PrecioU, ru.Id_Rubro, ru.Descripcion Rubrodesc,p.CodigoEspecial
			from FacturaCabeceraB fc
				join FacturaCuerpoB fd  on fc.IDAutomatico = fd.FacturaID
				join productos p on fd.ProductoID = p.UniqueID
				join rubros_cab ru on p.Id_Rubro =ru.Id_Rubro
			where 
		  isnull(fc.Estado,0) = 0
				and cast(fc.Fecha as date) between @FechaD and @FechaH

				and p.Id_Rubro = 10
		group by  p.UniqueID, p.PNombre, DATEPART(MONTH , cast(fc.fecha as date)), ru.Id_Rubro, ru.Descripcion ,p.CodigoEspecial
 ) S
 PIVOT
 ( 
 sum(S.cantidad)  for s.mes in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
 ) piv

 union all

 -- Venta Actual

 select
			p.UniqueID, p.PNombre,0,0,0,0,0,0,0,0,0,0,0,0,0, sum(fd.cantidad)Venta_Actual

			,0 compra,0 ProvID,'' ProvNom,0 id_rubro,'' RubroDesc,0 PrecioC, 0 PrecioU, 0 CodigoEspecial

			from FacturaCabeceraB fc
				join FacturaCuerpoB fd  on fc.IDAutomatico = fd.FacturaID
				join productos p on fd.ProductoID = p.UniqueID
			where 
		  isnull(fc.Estado,0) = 0
				and DATEPART(MONTH,fc.Fecha) = DATEPART(MONTH, getdate())
				and p.Id_Rubro = 10
				GROUP BY p.UniqueID, p.PNombre 

union all
			SELECT	p.UniqueID,p.pnombre

					,0,0,0,0,0,0,0,0,0,0,0,0,0,0 Venta_Actual
			
					,sum(cantidad) Cantidad,prov.UniqueID ProvID
				
					,prov.Empresa ProvNom,  r.id_Rubro, r.Descripcion RubroDesc, AVG(cd.PrecioU) precioC, 0 PrecioU, 0 CodigoEspecial
 
				FROM ComprasCabecera cc join ComprasCuerpo cd on cc.IDAutomatico = cd.CompraID
				               	join productos p on cd.ProductoID = p.UniqueID
								join Proveedores prov on cc.ProveedorID = prov.UniqueID
								join Rubros_Cab r on  p.Id_Rubro = r.Id_Rubro 
				where isnull(cc.Estado,0) = 0
						and cast(cc.Fecha as date) between @FechaD and @FechaH
						and p.id_rubro = 10
	   
				group by p.UniqueID,p.pnombre, prov.UniqueID, prov.Empresa,r.id_Rubro, r.Descripcion
	
) T group by ProductoID, PNombre

order by ProductoID

SET NOCOUNT OFF