select UniqueID,pnombre 
		porvID,Empresa ProvNom, Id_Rubro,RubroDesc,[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],PrecioC, PrecioU, CodigoEspecial
		  from
(
SELECT datepart(month,fecha ) mes ,p.UniqueID,p.pnombre, prov.UniqueID ProvID
		,prov.Empresa, ru.Id_Rubro, ru.Descripcion RubroDesc,  sum(cantidad) Cantidad, p.PrecioC, p.PrecioU, p.CodigoEspecial

FROM ComprasCabecera cc join ComprasCuerpo cd on cc.IDAutomatico = cd.CompraID
						join Productos p on cd.ProductoID = p.UniqueID
						join Proveedores prov on cc.ProveedorID = prov.UniqueID
						join rubros_cab ru on p.Id_Rubro =ru.Id_Rubro

 where isnull(cc.Estado,0) = 0
       and cast(cc.Fecha as date) between '20190101' and '20190401'
	   
	  group by datepart(month,fecha),p.UniqueID,p.pnombre, prov.UniqueID, prov.Empresa, 
							ru.Id_Rubro, ru.Descripcion, p.PrecioC, p.PrecioU,p.CodigoEspecial
) s
pivot
(
 sum(cantidad) for s.mes in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
) piv