
use DamianGauto
go
declare @fechaD as varchar(20) = '26/12/2018', @fechaH as varchar(20) = '27/12/2018' , @vendedor as int =  3

Select t1.fecha,t1.venid, sum(t1.contado) contado ,sum(t1.credito) credito,
		sum(t1.descuento)descuento,sum(t1.gasto) gasto,sum(t1.transf) tranf
		,sum(t1.retencion) retencion ,sum(t1.efectivo) efectivo, sum(t1.cheque) cheque, sum(remision)remision

from(	
	Select  cast(fc.Fecha as date) fecha,fc.vendedorid venid, 
	iif(fc.Pago = 0, sum(fc.total),0) contado, 
	iif(fc.Pago > 0, sum(fc.total),0) credito,
	sum(fc.descuento)descuento,
	0 gasto, 0 transf,
    0 retencion,0 efectivo ,0 cheque, 0 remision
	from facturacabeceraB fc


	where ISNULL(fc.ESTADO,0)=0  and cast(fc.Fecha as date) between @fechaD and @fechaH 
	AND FC.VendedorId = @vendedor
	group by cast(fc.fecha as date),fc.Pago,VendedorId

	
	union all
	
	SELECT cast(ga.fecha as date),ga.VendedorId,0,0,0, sum(ga.total),0,0,0,0,0
	FROM Gastos_Cab ga 
		WHERE  ISNULL(ga.ESTADO,0)= 0 and cast(ga.fecha as date) BETWEEN  @fechaD and @fechaH
		AND GA.VendedorId = @vendedor
		group by cast(ga.fecha as date), ga.VendedorId
 
	union all

	 SELECT   cast(tr.fecha as date) ,tr.VendedorId,0,0,0,0,sum(tr.total),0,0,0,0
	 FROM Transferencias_Cab tr 
		WHERE   ISNULL(tr.ESTADO,0)=0 and cast(tr.fecha as date) BETWEEN  @fechaD and @fechaH
		AND TR.VendedorId = @vendedor
		group by cast(fecha as date) ,tr.vendedorid

	union all

	SELECT cast(a.Fecha_A as Date), a.Id_Vendedor,0,0,0,0,0,0,
			IIF(ID_VALOR = 19, sum(a.total),0),
			IIF(ID_VALOR = 11, sum(a.total),0),
			IIF(ID_VALOR = 13, sum(a.total),0)

	 FROM ARQUEOS a
			INNER JOIN  Arqueos_Det ad ON a.Id_Arqueo = ad.Id_Arqueo 
			where isnull(a.estado,0) = 0 AND A.Id_Vendedor = @vendedor 
			AND cast(A.Fecha_A as date) BETWEEN  @fechaD and @fechaH
			AND AD.Id_Valor IN (11,13,19)
	GROUP BY A.Fecha_A,A.Id_Vendedor,AD.Id_Valor

	union all

	Select cast(rc.FechaInicio as date), rc.VendedorId,0,0,0,0,0,0,0,0, sum(rc.Total) 
	from  RemisionCabecera rc 
    WHERE ISNULL(rc.ESTADO,0)=0 and rc.VendedorId = @vendedor  and cast(rc.FechaInicio as date)  BETWEEN  @fechaD and @fechaH
	GROUP BY cast(rc.FechaInicio as date), RC.VendedorId


) T1

group by t1.fecha , t1.venid -- ,t1.credito, t1.contado,t1.descuento,t1.gasto, t1.transf

