 
 DECLARE @FechaD as Varchar(20)= '20190113', @FechaH as Varchar(20) = '20190314' 

 select * from
 (
  select  
		DATEPART ( MONTH , cast(fc.fecha as date) ) Mes,SUM(fd.Cantidad) Cantidad 
		from FacturaCabeceraB fc
		join FacturaCuerpoB fd  on fc.IDAutomatico = fd.FacturaID
	where 
		  isnull(fc.Estado,0) = 0
	  and fc.Fecha between @FechaD and @FechaH
	group by DATEPART ( MONTH , cast(fc.fecha as date) )
 ) S
 pivot
 ( 
 sum(S.cantidad)  for s.mes in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
 ) piv


