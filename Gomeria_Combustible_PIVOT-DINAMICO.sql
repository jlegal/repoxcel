USE GYG_SA
/*
select fecha,VendedorId, sum(gomeria) Gomeria, sum(combustible) Combustible 
from 
(
SELECT 
cast(gc.fecha as date) Fecha, GC.VendedorId,
iif( gd.ProductoID = 98,sum( gc.total),0) Gomeria,
iif( gd.ProductoID = 99,sum( gc.total),0) Combustible
FROM Gastos_Cab GC 
		JOIN Gastos_Det GD ON GC.IDAutomatico = GD.GastoId
where isnull(gc.Estado,0) = 0
	  and gc.VendedorId = 14
	  and gc.fecha between '20/01/2019' and '05/02/2019'
	  and gd.ProductoID in(98,99)
group by cast(gc.fecha as date), gd.ProductoID,GC.VendedorId
)T
group by fecha,VendedorId
*/

/*
select fecha, sum(gomeria1) Gomeria1, sum(combustible1) Combustible1,
sum(gomeria1) Gomeria2, sum(Combustible2) Combustible2,
sum(gomeria1) Gomeria3, sum(Combustible3) Combustible3,
sum(gomeria1) Gomeria4, sum(Combustible4) Combustible4,
sum(gomeria1) Gomeria5, sum(Combustible5) Combustible5,
sum(gomeria1) Gomeria6, sum(Combustible6) Combustible6,
sum(gomeria1) Gomeria7, sum(Combustible7) Combustible7,
sum(gomeria1) Gomeria8, sum(Combustible8) Combustible8,
sum(gomeria1) Gomeria9, sum(Combustible9) Combustible9,
sum(gomeria1) Gomeria10, sum(combustible10) Combustible10 
from 
(
SELECT 
cast(gc.fecha as date) Fecha, GC.VendedorId,
iif( gd.ProductoID = 98 and gc.vendedorId = 1,sum( gc.total),0) Gomeria1,
iif( gd.ProductoID = 99 and gc.vendedorId = 1,sum( gc.total),0) Combustible1,
iif( gd.ProductoID = 98 and gc.vendedorId = 2,sum( gc.total),0) Gomeria2,
iif( gd.ProductoID = 99 and gc.vendedorId = 2,sum( gc.total),0) Combustible2,
iif( gd.ProductoID = 98 and gc.vendedorId = 3,sum( gc.total),0) Gomeria3,
iif( gd.ProductoID = 99 and gc.vendedorId = 3,sum( gc.total),0) Combustible3,
iif( gd.ProductoID = 98 and gc.vendedorId = 4,sum( gc.total),0) Gomeria4,
iif( gd.ProductoID = 99 and gc.vendedorId = 4,sum( gc.total),0) Combustible4,
iif( gd.ProductoID = 98 and gc.vendedorId = 5,sum( gc.total),0) Gomeria5,
iif( gd.ProductoID = 99 and gc.vendedorId = 5,sum( gc.total),0) Combustible5,
iif( gd.ProductoID = 98 and gc.vendedorId = 6,sum( gc.total),0) Gomeria6,
iif( gd.ProductoID = 99 and gc.vendedorId = 6,sum( gc.total),0) Combustible6,
iif( gd.ProductoID = 98 and gc.vendedorId = 7,sum( gc.total),0) Gomeria7,
iif( gd.ProductoID = 99 and gc.vendedorId = 7,sum( gc.total),0) Combustible7,
iif( gd.ProductoID = 98 and gc.vendedorId = 8,sum( gc.total),0) Gomeria8,
iif( gd.ProductoID = 99 and gc.vendedorId = 8,sum( gc.total),0) Combustible8,
iif( gd.ProductoID = 98 and gc.vendedorId = 9,sum( gc.total),0) Gomeria9,
iif( gd.ProductoID = 99 and gc.vendedorId = 9,sum( gc.total),0) Combustible9,
iif( gd.ProductoID = 98 and gc.vendedorId = 10,sum( gc.total),0) Gomeria10,
iif( gd.ProductoID = 99 and gc.vendedorId = 10,sum( gc.total),0) Combustible10

FROM Gastos_Cab GC 
		JOIN Gastos_Det GD ON GC.IDAutomatico = GD.GastoId
where isnull(gc.Estado,0) = 0
	  and gc.VendedorId in (1,2,3,4,5,6,7,8,9,10)
	  and gc.fecha between '20/01/2019' and '05/02/2019'
	  and gd.ProductoID in(98,99)
group by cast(gc.fecha as date),GC.VendedorId, gd.ProductoID
)T
group by fecha
*/
--CREATE OR ALTER PROCEDURE AdelantosVendedor @FechaD nvarchar(20), @FechaH nvarchar(20) AS       

DECLARE  @FechaD nvarchar(20)='20190226', @FechaH nvarchar(20)='20190226' 
DECLARE @cols AS NVARCHAR(MAX),
		@query  AS NVARCHAR(MAX);

SET @cols = STUFF((SELECT distinct ',' + QUOTENAME(m.Descripcion)
			from moviles m
			FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)'),1,1,'')

set @query = ' SELECT *
				FROM
(
  select cast(dc.Fecha as date) Fecha,dd.Descripcion,p.nombres,sum(dc.total) total from Debitos_Cab dc
		join Debitos_det dd on dc.Id_Debito = dd.Id_Debito 
		join Personas p  on dc.Id_Persona = p.Id_Persona
  where isnull(dc.Estado,0) = 0
		AND cast(dc.Fecha as date) between ''' + @FechaD + ''' and ''' + @FechaH + '''
	group by dc.fecha,dd.Descripcion,p.nombres
) AS s 
 PIVOT
 (
 sum(s.Total) FOR s.nombres IN(' + @cols + ')
 ) AS pip 
 order by Fecha;'

 execute(@query)
